% Created 2019-11-20 mer. 16:08
\documentclass[small]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\graphicspath{{./imgs/}}
\usetheme{Madrid}
\usecolortheme{seagull}
\author{Cédric Doutriaux}
\date{\today}
\title{Cours Impression 3D}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 25.2.2 (Org mode 8.2.10)}}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}






\section{histoire et tour d'horizon}
\label{sec-1}


\subsection{petite histoire de l'impression 3D}
\label{sec-1-1}


\begin{frame}[label=sec-1-1-1]{1984 : cocorico !}
Alain le Méhauté (Alcatel), Olivier de Witte (Cilas) et Jean-Claude André  (CNRS)
la stéréolithographie.
\includegraphics[width=0.6\textwidth]{./Brevet_alcatel_cilas.jpg}
\url{https://bases-brevets.inpi.fr/fr/document/FR2567668/publications.html}
Peu convaincus de l’intérêt de cette technique Alcatel et Cilas abandonneront rapidement ces brevets.
\end{frame}

\begin{frame}[label=sec-1-1-2]{1986 : american dream}
la première machine de stéréolithographie commerciale,
 le procédé est breveté par Chuck Hull en 1986 et diffusé par son entreprise 3D Systems. 
\includegraphics[width=0.8\textwidth]{./chuckHull.jpeg}
\url{https://www.3dsystems.com}
\url{https://www.youtube.com/watch?v=NpRDuJ5YgoQ}
\end{frame}

\begin{frame}[label=sec-1-1-3]{1988 : SLS}
En 1988, une nouvelle technologie nommée frittage par laser ou encore SLS (Selective Laser Sintering) a été brevetée.
Ce brevet fut déposé par une université du Texas CARL DECKARD. la première machine par frittage sélectif par laser, 
elle fut créée en 1988 par DTM INC puis rachetée par 3D Systems
\includegraphics[width=0.4\textwidth]{./US4938816-SLS.jpg}
\end{frame}

\begin{frame}[label=sec-1-1-4]{1992 : FDM}
la société Stratasys dépose un brevet sur le procédé FDM (Fused Deposition Modeling)
qui consiste à construire un objet à partir de plastique fondu et extrudé en couches successives.
C'est ce procédé qu'utilise une RepRap sous le nom de (FFF* : Fuse Filament Fabrication).
\includegraphics[width=0.5\textwidth]{./US5121329-FFF.jpg}

\url{https://www.stratasys.com/}
\url{https://patents.google.com/patent/US5121329A/en}
\end{frame}



\begin{frame}[label=sec-1-1-5]{1995 :  3DP}
Z Corporation lance les premières imprimantes 3DP

\includegraphics[width=0.6\textwidth]{./binder-jetting.jpg}


ou le SLA (Stéréolithographie Apparatus) utilisant de la résine photo-polymérisante sensible aux ultras-violets.



Tous ces systèmes utilisés par les entreprises restent assez coûteux (en machines, comme en consommables),
 et sont donc de ce fait restés inaccessibles au commun des mortels. 
Il fallut attendre 2005 et le projet RepRap pour diffuser cette technologie au plus grand nombre.
\end{frame}


\begin{frame}[label=sec-1-1-6]{Reprap !}
en 2005 , Andrian Bower profite de la tombée en domaine public du brevet de FDM  pour concevoir une machine peu couteuse , autoréplicable et opensource.
\end{frame}


\begin{frame}[label=sec-1-1-7]{mainstream}
en 2013, Barrack Obama prononce un discours éloge de l'impression 3D, décrivant cette technique comme le levier d'une révolution technologique.
\url{https://www.youtube.com/watch?v=TPSkwndBUpQ}

« Un entrepôt autrefois abandonné est devenu un labo de pointe où les employés maitrisent l'impression 3D,
 procédé qui a le potentiel de révolutionner la façon dont nous fabriquons pratiquement tous les objets. » Barack Obama, 2013 (0:37)
\end{frame}


\begin{frame}[label=sec-1-1-8]{references}
\begin{block}{histoire}
\url{https://fr.flossmanuals.net/reprap/historique/}
\url{https://www.sculpteo.com/blog/fr/2017/05/22/lhistoire-de-limpression-3d-les-technologies-dimpression-3d-des-annees-80-a-nos-jours/}
\url{https://formlabs.com/fr/blog/histoire-impression-3d-stereolithographie/}
\url{https://www.supinfo.com/articles/single/34-histoire-impression-3d}
\url{https://3dprinting.com/what-is-3d-printing/#clip}

\url{https://www.stratasys.com/}

\url{http://www.primante3d.com/inventeur/}

\url{https://www.3dsystems.com}
\end{block}
\end{frame}



\subsection{tour d'horizon des différentes techniques}
\label{sec-1-2}

\begin{frame}[label=sec-1-2-1]{stereolythographie}
SLA
DLP
\url{https://formlabs.com/fr/blog/sla-dlp-impression-3D-comparee/}
\end{frame}



\section{exploration du projet Reprap}
\label{sec-2}

\section{vue d'ensemble d'une imprimante 3D FDM}
\label{sec-3}
\subsection{les différents composants}
\label{sec-3-1}
\subsection{maintenance de premier niveau}
\label{sec-3-2}
\subsection{customisation ou personnalisation}
\label{sec-3-3}
\section{chaîne de production}
\label{sec-4}

\subsection{outils libres de CAO}
\label{sec-4-1}


\begin{frame}[label=sec-4-1-1]{freecad}
initiation à freecad
\end{frame}

\begin{frame}[label=sec-4-1-2]{openscad}
initiation à openscad
\end{frame}


\subsection{le slicing (tranchage)}
\label{sec-4-2}

les outils disponibles
\begin{frame}[label=sec-4-2-1]{les principaux paramètres}
\begin{block}{épaisseur de couche}
\end{block}

\begin{block}{parois}
\end{block}

\begin{block}{remplissage}
\end{block}

\begin{block}{supports}
\end{block}

\begin{block}{adhésion au plateau}
\end{block}
\end{frame}

\subsection{l'impression}
\label{sec-4-3}

\begin{frame}[label=sec-4-3-1]{les différents matériaux}
le plus simple et courant : PLA

\url{https://www.simplify3d.com/support/materials-guide/properties-table/}
\end{frame}

\begin{frame}[label=sec-4-3-2]{points de vigilance}
\end{frame}

\begin{frame}[label=sec-4-3-3]{défauts courants et comment y remédier}
\end{frame}
% Emacs 25.2.2 (Org mode 8.2.10)
\end{document}
